CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers
 
INTRODUCTION
------------

The Entity Unique module checks entity uniqueness on form validation 
and on entity creation. Conditions what field/properties will be taken
in consideration to check entity uniqueness are fully customizable.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/entity_unique

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/entity_unique

REQUIREMENTS
------------

This module requires the following modules:

 * Entity API (https://www.drupal.org/project/entity)   
 
INSTALLATION
------------

Installation uses the standard Drupal process. If you're reading this,
you've likely already correctly installed this module.

 1. Download the module from drupal.org.
 2. Untar the archive into your modules directory.
 3. Go to Admin > Modules and enable the Entity Unique module.

CONFIGURATION
-------------

 * Configure settings at Admin > Structure > Content types 
   > Example content type name > Entity Unique for nodes.
 * Configure settings at Admin > Structure > Taxonomy 
   > Example vocabulary nam > Entity Unique for taxonomy terms. 
 
MAINTAINERS
-----------

Current maintainers:
 * Przemyslaw David Duczek (nimek) - https://www.drupal.org/user/350609
