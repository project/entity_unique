<?php

/**
 * @file
 * Contains administration forms.
 */

/**
 * Administration form for each bundle.
 */
function entity_unique_settings_form($form, $form_state, $entity_type, $bundle_arg) {
  $info = entity_get_info($entity_type);
  $bundle_name = is_object($bundle_arg) ? $bundle_arg->{$info['bundle keys']['bundle']} : $bundle_arg;
  $bundle_label = $info['bundles'][$bundle_name]['label'];
  $key = $entity_type . '_' . $bundle_name;
  $default_value = entity_unique_get_setting($key);
  $form['entity_unique'] = array(
    '#type' => 'fieldset',
    '#title' => t('Entity Unique for @type', array('@type' => $bundle_label)),
    '#weight' => 0,
  );
  $form['entity_unique']['entity_unique_' . $key] = array(
    '#type' => 'radios',
    '#title' => t('Scope'),
    '#default_value' => $default_value,
    '#options' => _entity_unique_options($entity_type, $bundle_name),
  );
  $form['entity_unique']['advanced'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Advanced settings'),
    '#tree' => FALSE,
  );
  $form['entity_unique']['advanced']['entity_unique_custom_message_' . $key] = array(
    '#type' => 'textfield',
    '#title' => t('Custom error message'),
    '#required' => FALSE,
    '#default_value' => entity_unique_get_custom_message($key, $entity_type),
    '#description' => t('Enter your custom error message here. Leve blank to use default.'),
  );
  $form['entity_unique']['advanced']['entity_unique_trim_info_' . $key] = array(
    '#type' => 'checkbox',
    '#title' => t('Trim value before validation and save.'),
    '#required' => FALSE,
    '#default_value' => entity_unique_get_trim_info($key, $entity_type),
    '#description' => t('Removes whitespaces from the begining and end of fields/properties values specified below. <b>Recommended!</b>'),
  );
  $form['entity_unique']['advanced']['entity_unique_presave_validation_' . $key] = array(
    '#type' => 'checkbox',
    '#title' => t('Validate before save.'),
    '#required' => FALSE,
    '#default_value' => entity_unique_get_presave_validation($key, $entity_type),
    '#description' => t('Adds additional validation on entity presave to prevent adding not unique entity programmatically.  <b>Experimental!</b> Not recommended on production sites.'),
  );
  $form['entity_unique']['advanced']['entity_unique_advanced_' . $key] = array(
    '#type' => 'checkboxes',
    '#title' => t('Specify uniqness using'),
    '#required' => TRUE,
    '#default_value' => entity_unique_get_advanced_setting($key, $entity_type),
    '#description' => t('Specify fields/properties with combination will be used to specify entity uniqness. This has no effect if scope is disabled.'),
    '#options' => _entity_unique_advanced_options($entity_type, $bundle_name),
  );
  return system_settings_form($form);
}

/**
 * Constructs the list of options for the given bundle.
 */
function _entity_unique_options($entity_type, $bundle_name) {
  $options = array(
    ENTITY_UNIQUE_DISABLED => t('Disabled'),
    ENTITY_UNIQUE_PER_ENTITY_TYPE => t('<strong>Uniqueness per entity type</strong>. Example term name must be unique among all taxonomy terms from all vocabularies'),
    ENTITY_UNIQUE_PER_ENTITY_BUNDLE => t('<strong>Uniqueness per entity bundle</strong>. Example term name must be unique among all taxonomy terms from given vocabulary'),
  );
  // Remove uniqueness per entity bundle setting for bundlesless entities like users.
  // For that kind of entities entity type is the same as bundle name.
  if ($entity_type == $bundle_name) {
    unset($options[ENTITY_UNIQUE_PER_ENTITY_BUNDLE]);	  
  }
  return $options;
}

/**
 * Constructs the list of advanced options for the given bundle.
 */
function _entity_unique_advanced_options($entity_type, $bundle_name) {
  $info = entity_get_info($entity_type);
  $property_info = entity_get_property_info($entity_type);
  $property_info = $property_info['properties'];
  $field_info = field_info_instances($entity_type, $bundle_name);
  $unwanted_properties = $info['entity keys'];
  // Entity label stays so we remowe it from unwanted array.
  unset($unwanted_properties['label']);
  $unwanted_properties += array(
    'is_new',
    'url',
    'edit_url',
    'log',
    'body',
    'revision',
    'source',
    'commerce_price',
    'description',
    'vocabulary',
  );
  $unwanted_fields = array('body', 'commerce_price');
  $correct_properties = array_diff(array_keys($property_info), $unwanted_properties);
  $correct_fields = array_diff(array_keys($field_info), $unwanted_fields);
  foreach ($correct_properties as $value) {
    $options[$value] = $property_info[$value]['label'] . " (" . $value . ")";
  }
  foreach ($correct_fields as $value) {
    $options[$value] = $field_info[$value]['label'] . " (" . $value . ")";
  }
  return $options;
}
